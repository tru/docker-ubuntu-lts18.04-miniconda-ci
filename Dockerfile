FROM registry-gitlab.pasteur.fr/tru/docker-ubuntu-lts18.04-ci:latest
MAINTAINER Tru Huynh <tru@pasteur.fr>

RUN apt-get update && DEBIAN_FRONTEND=noninteractive  apt-get -y upgrade
# bzip2 and curl requirements 
RUN apt-get update && DEBIAN_FRONTEND=noninteractive  apt-get -y install bzip2 curl

# install miniconda
RUN curl -qsSLkO https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
&& bash Miniconda3-latest-Linux-x86_64.sh -b -p /opt/miniconda3 \
&& rm Miniconda3-latest-Linux-x86_64.sh
RUN /opt/miniconda3/bin/conda update conda && /opt/miniconda3/bin/conda update --all

# environment
ENV PATH=/opt/miniconda3/bin:$PATH

RUN date +"%Y-%m-%d-%H%M" > /last_update
