# building a miniconda minimal image based on Ubuntu LTS 18.04 for CI

Tru <tru@pasteur.fr>

(toy) docker image produced available at `registry-gitlab.pasteur.fr/tru/docker-ubuntu-lts18.04-miniconda-ci:latest`

# singularity image building

`singularity build docker-ubuntu-lts18.04-miniconda-ci.sif docker://registry-gitlab.pasteur.fr/tru/docker-ubuntu-lts18.04-miniconda-ci:latest`
